$reg64 = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*"
$reg32 = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"
$swdate = "20210504"
$swname = "iTunes*"

[array]$uwsw64 = Get-ItemProperty $reg64 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
[array]$uwsw32 = Get-ItemProperty $reg32 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
$uwsw = $uwsw64 += $uwsw32

foreach ($sw in $uwsw) {
    If ($sw.DisplayName -like $swname) {
        $realsw = Get-WmiObject -Class Win32_Product | Where-Object Name -eq $sw.DisplayName
        if ($realsw.name -eq $sw.DisplayName) {
            write-host "$($sw.DisplayName) will be uninstalled"
            if ($sw.UninstallString -like "MsiExec*") {
                if ($sw.UninstallString -like "*/I*") {
                    $uninst = $sw.UninstallString.Replace("/I","/X")
                } else {
                    $uninst = $sw.UnInstallString
                }
                cmd /c $uninst /quiet /norestart
            } else {
                $uninst = $sw.UnInstallString
                & $uninst
            }
        }
    }
}