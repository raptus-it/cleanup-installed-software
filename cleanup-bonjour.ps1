$swname = "Bonjour*"
$path = "C:\ProgramData\Apple\Installer Cache\*"
$startdate=[datetime]::ParseExact("04-05-2021 12:00","dd-MM-yyyy HH:mm",$null)
$enddate=[datetime]::ParseExact("04-05-2021 17:00","dd-MM-yyyy HH:mm",$null)

$swres = Get-WmiObject -Class Win32_Product | Where-Object Name -like $swname

foreach ($sw in $swres) {
    $validforremove = Get-Item $path | Where-Object {$_.CreationTime -gt $startdate -and $_.CreationTime -lt $enddate -and $_.Name -like $swname}
    if ($validforremove) {
        $sw.Uninstall()
    }
}