$swname = "WinSCP*"
$args = @("/VERYSILENT /NORESTART")
$uninst32 = "$env:ProgramFiles\WinSCP\unins000.exe"
$uninst64 = "${env:ProgramFiles(x86)}\WinSCP\unins000.exe"
$startdate=[datetime]::ParseExact("04-05-2021 12:00","dd-MM-yyyy HH:mm",$null)
$enddate=[datetime]::ParseExact("04-05-2021 17:00","dd-MM-yyyy HH:mm",$null)

if (Test-Path -Path $uninst32 -PathType Leaf) {
    $validforremove = Get-Item $uninst32 | Where-Object {$_.CreationTime -gt $startdate -and $_.CreationTime -lt $enddate}
    if ($validforremove) {
        write-host "$($swname) will be uninstalled"
        Start-Process -Filepath $uninst32 -ArgumentList $args
    }
} elseif (Test-Path -Path $uninst64 -PathType Leaf) {
    $validforremove = Get-Item $uninst64 | Where-Object {$_.CreationTime -gt $startdate -and $_.CreationTime -lt $enddate}
    if ($validforremove) {
        write-host "$($swname) will be uninstalled"
        Start-Process -Filepath $uninst64 -ArgumentList $args
    }
}