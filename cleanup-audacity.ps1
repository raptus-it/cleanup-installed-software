$reg64 = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*"
$reg32 = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"
$swdate = "20210504"
$swname = "Audacity*"
$args = @("/VERYSILENT", "/NORESTART")
$uninst32 = "$env:ProgramFiles\Audacity\unins000.exe"
$uninst64 = "${env:ProgramFiles(x86)}\Audacity\unins000.exe"

[array]$uwsw64 = Get-ItemProperty $reg64 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
[array]$uwsw32 = Get-ItemProperty $reg32 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
$uwsw = $uwsw64 += $uwsw32

foreach ($sw in $uwsw) {
    write-host "$($sw.DisplayName) will be uninstalled"
    if (Test-Path -Path $uninst32 -PathType Leaf) {
        Start-Process -Filepath $uninst32 -ArgumentList $args
    } elseif (Test-Path -Path $uninst64 -PathType Leaf) {
        Start-Process -Filepath $uninst64 -ArgumentList $args
    }
    exit 0
}