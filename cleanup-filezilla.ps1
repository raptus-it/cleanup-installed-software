$uninst32 = "$env:ProgramFiles\FileZilla FTP Client\uninstall.exe"
$uninst64 = "${env:ProgramFiles(x86)}\FileZilla FTP Client\uninstall.exe"
$startdate=[datetime]::ParseExact("04-05-2021 12:00","dd-MM-yyyy HH:mm",$null)
$enddate=[datetime]::ParseExact("04-05-2021 17:00","dd-MM-yyyy HH:mm",$null)
$swname = "Filezilla*"
$args = @("/S")

if (Test-Path -Path $uninst32 -PathType Leaf) {
    $validforremove = Get-Item $uninst32 | Where-Object {$_.CreationTime -gt $startdate -and $_.CreationTime -lt $enddate}
    if ($validforremove) {
        write-host "$($swname) will be uninstalled"
        Start-Process -Filepath $uninst32 -ArgumentList $args
    }
} elseif (Test-Path -Path $uninst64 -PathType Leaf) {
    $validforremove = Get-Item $uninst64 | Where-Object {$_.CreationTime -gt $startdate -and $_.CreationTime -lt $enddate}
    if ($validforremove) {
        write-host "$($swname) will be uninstalled"
        Start-Process -Filepath $uninst64 -ArgumentList $args
    }
}