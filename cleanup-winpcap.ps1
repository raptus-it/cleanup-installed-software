$swname = "WinPCAP*"
$args = @("/S")
$uninst32 = "$env:ProgramFiles\WinPcap\uninstall.exe"
$uninst64 = "${env:ProgramFiles(x86)}\WinPcap\uninstall.exe"

write-host "$($swname) will be uninstalled"
if (Test-Path -Path $uninst32 -PathType Leaf) {
    Start-Process -Filepath $uninst32 -ArgumentList $args
} elseif (Test-Path -Path $uninst64 -PathType Leaf) {
    Start-Process -Filepath $uninst64 -ArgumentList $args
}