$reg64 = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*"
$reg32 = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"
$swdate = "20210504"
$swname = "VNC*"

$url = "https://bitbucket.org/raptus-it/cleanup-installed-software/raw/master/sources/force-cleanup-vnctools.bat"
$dest = "C:\Windows\temp\force-cleanup-vnctools.bat"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]'Tls11,Tls12'
Invoke-WebRequest -Uri $url -OutFile $dest

[array]$uwsw64 = Get-ItemProperty $reg64 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
[array]$uwsw32 = Get-ItemProperty $reg32 | Select-Object DisplayName, UninstallString, InstallDate | Where-Object {$_.InstallDate -eq $swdate -and $_.DisplayName -like $swname}
$uwsw = $uwsw64 += $uwsw32

foreach ($sw in $uwsw) {
    If ($sw.DisplayName -like $swname) {
        $realsw = Get-WmiObject -Class Win32_Product | Where-Object Name -eq $sw.DisplayName
        if ($realsw.name -eq $sw.DisplayName) {
            write-host "$($sw.DisplayName) will be uninstalled"
            Start-Process -WindowStyle hidden "cmd.exe"  "/c $dest"
            Start-Sleep -s 5
            if ($sw.UninstallString -like "MsiExec*") {
                $uninst = $sw.UnInstallString
                cmd /c $uninst /quiet /norestart
            } else {
                $uninst = $sw.UnInstallString
                & $uninst
            }
            $realsw.Uninstall()
            exit 0
        }
    }
}
Remove-Item $dest