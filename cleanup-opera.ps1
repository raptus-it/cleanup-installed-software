$uninst32 = "$env:ProgramFiles\Opera\Launcher.exe"
$uninst64 = "${env:ProgramFiles(x86)}\Opera\Launcher.exe"
$swname = "Opera*"
$args = @("/uninstall", "/silent")

write-host "$($swname) will be uninstalled"
if (Test-Path -Path $uninst32 -PathType Leaf) {
    Start-Process -Filepath $uninst32 -ArgumentList $args
} elseif (Test-Path -Path $uninst64 -PathType Leaf) {
    Start-Process -Filepath $uninst64 -ArgumentList $args
}